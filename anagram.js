var utils = require('./utils');

var inputA = utils.getRandomString(1000000);
var inputB = inputA.split('').reverse().join('');
var inputC = utils.getRandomString(1000000);



// O(n^2) due to sort
var isAnagramSort = function(strA, strB) {
  if (strA.length !== strB.length)
    return false
  var sortedA = strA.split('').sort();
  var sortedB = strB.split('').sort();
  for (var x = 0; x < sortedA.length; x++) {
    if (sortedA[x] !== sortedB[x])
      return false;
  }
  return true;
}

console.time('isAnagramSort');
console.log('isAnagramSort', isAnagramSort(inputA, inputB));
console.timeEnd('isAnagramSort');



// O(n)
var isAnagramCount = function(strA, strB) {
  if (strA.length !== strB.length)
    return false
  var countA = {};
  var countB = {};
  var char;
  for (var x = 0; x < strA.length; x++) {
    char = strA[x];
    if (countA[char] === undefined)
      countA[char] = 0;
    countA[char]++;
  }
  for (var x = 0; x < strB.length; x++) {
    char = strB[x];
    if (countB[char] === undefined)
      countB[char] = 0;
    countB[char]++;
  }
  for (var char in countA) {
    if (countA[char] !== countB[char])
      return false;
  }
  return true;
}

console.time('isAnagramCount');
console.log('isAnagramCount', isAnagramCount(inputA, inputB));
console.timeEnd('isAnagramCount');
