var factorialIterative = function(x) {
  var result = x;
  while (x > 1) {
    result = result * (x - 1);
    x--;
  }
  return result;
}

console.time('factorialIterative');
console.log('factorialIterative', factorialIterative(20));
console.timeEnd('factorialIterative');



var factorialRecursive = function(x) {
  if (x === 1) return x;
  return x * factorialRecursive(x - 1);
}

console.time('factorialRecursive');
console.log('factorialRecursive', factorialRecursive(20));
console.timeEnd('factorialRecursive');
