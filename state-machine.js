var isValidHtml = function(str) {

  var STATES = {
    OPEN_TAG:    'open-tag',
    CLOSE_TAG:   'close-tag',
    CLOSE_SLASH: 'close-slash',
    TAG_NAME:    'tag-name',
    ERROR:       'error'
  };

  var VALID_TRANSITIONS = {};
  VALID_TRANSITIONS[STATES.OPEN_TAG] =     [STATES.TAG_NAME, STATES.CLOSE_SLASH];
  VALID_TRANSITIONS[STATES.CLOSE_TAG] =    [STATES.OPEN_TAG];
  VALID_TRANSITIONS[STATES.CLOSE_SLASH] =  [STATES.TAG_NAME];
  VALID_TRANSITIONS[STATES.TAG_NAME] =     [STATES.TAG_NAME, STATES.CLOSE_TAG];

  var getStateForChar = function(char) {
    var state;
    switch (char) {
      case '<':
        state = STATES.OPEN_TAG;
        break;
      case '>':
        state = STATES.CLOSE_TAG;
        break;
      case '/':
        state = STATES.CLOSE_SLASH;
        break;
      default:
        state = STATES.TAG_NAME;
        break;
    }
    return state;
  };

  var openTags = 0;

  var getNextState = function(fromState, toState) {
    var validTransitions = VALID_TRANSITIONS[fromState];
    if (validTransitions.indexOf(toState) !== -1)
      return toState;
    else
      return STATES.ERROR;
  };

  var trackTransition = function(fromState, toState) {
    if (fromState === STATES.OPEN_TAG && toState === STATES.TAG_NAME)
      openTags++;
    if (fromState === STATES.OPEN_TAG && toState === STATES.CLOSE_SLASH)
      openTags--;
  };

  var currentState, nextState;
  for (var position = 0; position < str.length; position++) {
    currentState = getStateForChar(str[position]);
    nextState = getStateForChar(str[position+1]);
    trackTransition(currentState, nextState);
    console.log(str[position], currentState, nextState, openTags);
    currentState = getNextState(currentState, nextState);
  }

  return openTags === 0;

};



var validHtml = "<html><body><div></div></body></html>";
var invalidHtml1 = "<html><body><div></div></html>"; // no closing body
var invalidHtml2 = "<html><body><div><div></body></html>"; // incorrect spell div

console.log(isValidHtml(validHtml)); // true
console.log(isValidHtml(invalidHtml1)); // false
console.log(isValidHtml(invalidHtml2)); // false
