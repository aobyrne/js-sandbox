var lookAndSayIterative = function(digits) {
  var result = '';
  var input = String(digits).split('');
  var count;

  while (input.length > 0) {
    count = 1;
    while (input[0] === input[count])
      count++;
    result += count + '' + input[0];
    input = input.slice(count)
  }

  return result;
};

console.time('lookAndSayIterative');
console.log('lookAndSayIterative', lookAndSayIterative(1122212));
console.timeEnd('lookAndSayIterative');



var lookAndSayRecursive = function(digits) {
  var result = '';
  var input = String(digits).split('');
  var count;

  var lookAndSay = function(input) {
    if (input.length === 0)
      return result;

    count = 1;
    while (input[0] === input[count])
      count++;
    result += count + '' + input[0];
    input = input.slice(count);

    return lookAndSay(input);
  };

  return lookAndSay(input);
};

console.time('lookAndSayRecursive');
console.log('lookAndSayRecursive', lookAndSayRecursive(1122212));
console.timeEnd('lookAndSayRecursive');
