module.exports = {

  getSortedNumbers: function(length) {
    return Array.from(Array(length).keys());
  },

  getUnsortedNumbers: function(length) {
    return Array.from(Array(length).keys()).sort(function() {
      return 0.5 - Math.random();
    });
  },

  getRandomString: function(length) {
    var alphabet = "abcdefghijklmnopqrstuvwxyz";
    return Array(length)
      .join()
      .split(',')
      .map(function() {
        return alphabet.charAt(Math.floor(Math.random() * alphabet.length));
      })
      .join('');
  }

};
