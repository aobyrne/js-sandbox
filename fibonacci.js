var fibonacciIterativeMemoize = function(n) {
  var cache = {};
  for (var i = 0; i <= n; i++) {
    if (i === 0 || i === 1) {
      cache[i] = i;
    } else {
      cache[i] = cache[i-1] + cache[i-2];
    }
  }
  return cache[n];
};

console.time('fibonacciIterativeMemoize');
console.log(fibonacciIterativeMemoize(41));
console.timeEnd('fibonacciIterativeMemoize');




var cache = {};
var fibonacciRecursiveMemoize = function(n) {
  if (cache[n] !== undefined) {
    return cache[n];
  }
  if (n === 0) {
    cache[n] = 0;
    return 0;
  }
  if (n === 1) {
    cache[n] = 1;
    return 1;
  }
  cache[n] = (fibonacciRecursiveMemoize(n-1) + fibonacciRecursiveMemoize(n-2));
  return cache[n];
};

console.time('fibonacciRecursiveMemoize');
console.log(fibonacciRecursiveMemoize(41));
console.timeEnd('fibonacciRecursiveMemoize');




var fibonacciRecursive = function(n) {
  if (n === 0 || n === 1) return n;
  return (fibonacciRecursive(n-1) + fibonacciRecursive(n-2));
};

console.time('fibonacciRecursive');
console.log(fibonacciRecursive(41));
console.timeEnd('fibonacciRecursive');
