var LinkedListNode = function(value) {
  this.data = value;
  this.next = null;
};

LinkedListNode.prototype.toString = function() {
  return this.data;
};

var LinkedList = function() {
  this.head = null;
};

LinkedList.prototype.get = function(position) {
  var currentNode = this.head;
  var counter = 1;
  while (currentNode) {
    if (position === counter)
      return currentNode;
    currentNode = currentNode.next;
    counter++;
  }
  return null;
};

LinkedList.prototype.append = function(value) {
  var newNode = new LinkedListNode(value);
  if (this.head === null) {
    this.head = newNode;
  } else {
    var currentNode = this.head;
    while (currentNode.next)
      currentNode = currentNode.next;
    currentNode.next = newNode;
  }
  return newNode;
};

LinkedList.prototype.insert = function(value, position) {
  var newNode = new LinkedListNode(value);
  if (position === 1) {
    newNode.next = this.head;
    this.head = newNode;
  } else {
    var currentNode = this.head;
    var previousNode = null;
    var counter = 1;
    while (counter < position) {
      previousNode = currentNode;
      currentNode = currentNode.next;
      counter++;
    }
    previousNode.next = newNode;
    newNode.next = currentNode;
  }
  return newNode;
};

LinkedList.prototype.delete = function(position) {
  if (position === 1) {
    this.head = this.head.next;
  } else {
    var currentNode = this.head;
    var previousNode = this.head;
    var counter = 1;
    while (counter < position) {
      previousNode = currentNode;
      currentNode = currentNode.next;
      counter++;
    }
    previousNode.next = currentNode.next;
  }
};

LinkedList.prototype.nodes = function() {
  var nodes = [];
  var currentNode = this.head;
  while (currentNode) {
    nodes.push(currentNode);
    currentNode = currentNode.next;
  }
  return nodes;
};


var assert = require('chai').assert;

var list = new LinkedList();
assert.equal(list.head, null);

list.append('apple');
list.append('banana');
list.append('grapes');
// apple, banana, grapes
assert.equal(list.nodes().length, 3);

list.insert('orange', 2);
// apple, orange, banana, grapes
assert.equal(list.get(2).data, 'orange');
assert.equal(list.get(3).data, 'banana');
assert.equal(list.nodes().length, 4);

list.delete(1);
// orange, banana, grapes
list.insert('mandarin', 1);
// mandarin, orange, banana, grapes
list.delete(3);
// mandarin, orange, grapes
assert.equal(list.get(2).data, 'orange');
assert.equal(list.nodes().length, 3);

console.log(list.nodes().map(node => node.data));
