var stringPermutations = function(input) {
  var index = 0;

  var merge = function(char, permutation) {
    // console.log(char);
    // c, ['ab', 'ba']
    // cab, acb, abc
    if (permutation.length === 0) return [char];
    var permutations = [];
    for (var i = 0; i < permutation.length; i++) {
      var arr = permutation[i].split('');
      for (var x = 0; x <= arr.length; x++) {
        var copy = arr.slice();
        // console.log(copy);
        copy.splice(x, 0, char)
        permutations.push(copy.join(''));
      }
    }
    return permutations;
  }

  var char;
  var index = 0;
  var permute = function(permutation) {
    if (index === input.length)
      return permutation;
    char = input[index];
    index++;
    return permute(merge(char, permutation));
  };

  return permute([]);

  // console.log(merge('a', []));
  // console.log(merge('b', ['a']));
  // console.log(merge('c', ['ab', 'ba']));

  // console.log(merge('c', merge('b', merge('a', []))));
};

console.time('stringPermutations');
console.log(stringPermutations(['a', 'b', 'c']));
console.timeEnd('stringPermutations');
