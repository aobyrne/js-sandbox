var binaryCount = function(list, value) {

  var first = function() {
    var low = 0;
    var high = list.length - 1;
    var mid;
    var search = function(low, high) {
      if (low > high) return low;
      mid = Math.floor((low + high) / 2);
      if (list[mid] < value)
        return search(mid+1, high);
      else
        return search(low, mid-1);
    };
    return search(low, high);
  };

  var last = function() {
    var low = 0;
    var high = list.length - 1;
    var mid;
    var search = function(low, high) {
      if (low > high) return low;
      mid = Math.floor((low + high) / 2);
      if (list[mid] > value)
        return search(low, mid-1);
      else
        return search(mid+1, high);
    };
    return search(low, high);
  };

  return last() - first();

};


var list = [1, 1, 2, 2, 2, 3, 3, 3, 4, 5];

console.log(binaryCount(list, 2));
