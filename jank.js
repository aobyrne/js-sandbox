var box1 = document.getElementById('box1');
var box2 = document.getElementById('box2');


var createMovement = function(el, speed) {
	var left = 0;
  var direction = 1;
	return function() {
  	if (left > 200 || left < 0) direction *= -1;
    left += speed * direction;
		el.style.left = left + 'px';
  };
};

animateJanky = function(box, speed) {
	var move = createMovement(box, speed);
	var movement = function() {
  	move();
  };
  setInterval(movement, 16);
};

animateSmooth = function(box, speed) {
	var move = createMovement(box, speed);
  var movement = function() {
  	move();
    requestAnimationFrame(movement);
  };
	requestAnimationFrame(movement);
};

animateJanky(box1, 2);
animateSmooth(box2, 3);
