var obj = [{id:1, limit:10}, {id:2, limit:5}, {id:3, limit: 7}];

// partially configure with the prop, provide obj later
var getProp = function(prop) {
	return function(obj) {
  	return obj[prop];
  }
};

var getIds = getProp('id');
var ids = obj.map(getIds);
console.log(ids);

// partially configure with the mapping fn, provide obj later
var map = function(fn) {
	return function(obj) {
  	return obj.map(fn)
  }
}

var getLimits = map(getProp('limit'));
var limits = getLimits(obj);
console.log(limits);


/*
var doTask = function(action, item, day) {
	console.log(action + ' ' + item + ' ' + day);
}
*/

var doTask = function(action) {
	return function(item) {
  	return function(day) {
    	console.log(action + ' ' + item + ' ' + day);
    }
  }
};

var pickUp = doTask('pick up');
var eatGrapes = doTask('eat')('grapes');

pickUp('apples')('today');
eatGrapes('tomorrow');
doTask('carry')('oranges')('yesterday');
