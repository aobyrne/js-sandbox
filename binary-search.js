var utils = require('./utils');

var numbers = utils.getSortedNumbers(10000000);
var rotatedArray = numbers.concat(numbers.slice(0, 10));



var binarySearchIterative = function(list, value) {
  var low = 0;
  var high = list.length - 1;
  var mid;
  while (low <= high) {
    mid = Math.floor((low + high) / 2);
    if (list[mid] > value)
      high = mid - 1;
    else if (list[mid] < value)
      low = mid + 1;
    else
      return mid;
  }
  return -1;
};

console.time('binarySearchIterative');
binarySearchIterative(numbers.slice(), 11);
console.timeEnd('binarySearchIterative');



var binarySearchRecursive = function(list, value) {
  var low = 0;
  var high = list.length - 1;
  var mid;
  var search = function(low, high) {
    if (low > high) return -1;
    mid = Math.floor((low + high) / 2);
    if (list[mid] > value)
      return search(low, mid-1);
    if (list[mid] < value)
      return search(mid+1, high);
    return mid;
  };
  return search(low, high);
};

console.time('binarySearchRecursive');
binarySearchRecursive(numbers.slice(), 11);
console.timeEnd('binarySearchRecursive');



// O(n)
var pivotIterative = function(list) {
  for (var i = 0; i < list.length; i++) {
    if (i + 1 >= list.length)
      return -1;
    left = list[i];
    right = list[i+1];
    if (right < left)
      return i+1;
  }
}

console.time('pivotIterative');
console.log('[pivotIterative] pivot:', pivotIterative(rotatedArray));
console.timeEnd('pivotIterative');



// O(log n)
var pivotRecursiveBinary = function(list) {
  var left = 0;
  var right = Math.floor(list.length/2);
  var search = function(left, right) {
    if (list[right+1] < list[right]) {
      // pivot is the right-border of this range
      return right + 1;
    } else if (list[right] < list[left]) {
      // pivot is in this range
      if (right - left === 1) {
        // range contains 2 numbers, pivot found
        return right;
      }
      // otherwise search through first half of this range
      newLeft = left;
      newRight = right - Math.floor((right-left)/2);
    } else {
      // pivot must be in other half
      if (right - left <= 1) {
        // range contains 2 numbers, pivot not found
        return -1;
      }
      newLeft = right + 1;
      newRight = right + 1 + Math.floor((right-left)/2);
    }
    return search(newLeft, newRight);
  }
  return search(left, right);
}

console.time('pivotRecursiveBinary');
console.log('[pivotRecursiveBinary] pivot:', pivotRecursiveBinary(rotatedArray));
console.timeEnd('pivotRecursiveBinary');
