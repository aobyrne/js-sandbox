var utils = require('./utils')

var heapSort = function(input) {

  var swapArrayElements = function(arr, indexA, indexB) {
    var tmp = arr[indexA];
		arr[indexA] = arr[indexB];
		arr[indexB] = tmp;
    return;
  };

  var putArrayInHeapOrder = function(arr) {
    var count = input.length;
    var start = Math.floor(count - 2) / 2;
  	while (start >= 0) {
  		siftElementDownHeap(arr, start, count - 1);
  		start--;
  	}
    return;
  };

  var siftElementDownHeap = function(heap, root, end) {
    while ((root * 2 + 1) <= end) {
  		var child = root * 2 + 1;
  		if (child + 1 <= end && heap[child] < heap[child + 1])
  			child = child + 1;
  		if (heap[root] < heap[child]) {
        swapArrayElements(heap, root, child);
  			root = child;
  		} else {
  			return;
      }
  	}
    return;
  };

  putArrayInHeapOrder(input);

  var end = input.length - 1;
	while (end > 0) {
		swapArrayElements(input, 0, end);
		siftElementDownHeap(input, 0, end - 1);
		end--;
	};

  return input;
}

var smallInput = utils.getUnsortedNumbers(10);
var largeInput = utils.getUnsortedNumbers(100000);

console.time('heapSortSmall');
console.log('input:', smallInput);
console.log('output:', heapSort(smallInput));
console.timeEnd('heapSortSmall');

console.time('heapSortLarge');
heapSort(largeInput);
console.timeEnd('heapSortLarge');
