var createCounter = function(val) {
	var count = val || 0;
	return {
		increment: function() {
			count++;
    },
    decrement: function() {
      count--;
    },
    value: function() {
    	return count;
    }
	};
};

var counter1 = createCounter();
console.log(counter1.count); // undefined
counter1.increment();
counter1.increment();
console.log(counter1.value()); // 2
counter1.decrement();
console.log(counter1.value()); // 1

var counter2 = createCounter(10);
console.log(counter2.count); // undefined
counter2.increment();
console.log(counter2.value()); // 11
counter2.decrement();
console.log(counter2.value()); // 10
