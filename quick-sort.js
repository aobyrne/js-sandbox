var utils = require('./utils');

var randomNumbersSmall = utils.getUnsortedNumbers(10);
var randomNumbersLarge = utils.getUnsortedNumbers(100000);


console.time('internalSort');
randomNumbersLarge.slice().sort(function(a, b) { return a - b; });
console.timeEnd('internalSort');


var quickSort = function(input) {
  if (input.length < 2) return input;
  var pivot, lowPile, highPile;
  for (var i = 0; i < input.length; i++) {
    pivot = input[Math.floor(Math.random() * input.length)];
    lowPile = [];
    highPile = [];
    for (var x = 0; x < input.length; x++) {
      if (input[x] < pivot)
        lowPile.push(input[x]);
      else if (input[x] > pivot)
        highPile.push(input[x]);
    }
    return quickSort(lowPile).concat(pivot).concat(quickSort(highPile));
  }
};

console.time('quickSort');
quickSort(randomNumbersLarge.slice());
console.timeEnd('quickSort');



var quickSortFunctional = function(input) {
  if (input.length < 2) return input;
  var pivot, lowPile, highPile;
  for (var i = 0; i < input.length; i++) {
    pivot = input[Math.floor(Math.random() * input.length)];
    lowPile = [];
    highPile = [];
    input.forEach(function(num) {
      if (num < pivot)
        lowPile.push(num);
      else if (num > pivot)
        highPile.push(num);
    });
    return quickSortFunctional(lowPile).concat(pivot).concat(quickSortFunctional(highPile));
  }
};

console.time('quickSortFunctional');
quickSortFunctional(randomNumbersLarge.slice());
console.timeEnd('quickSortFunctional');
