var HashTable = function(sz) {
  var items = new Array(sz);
  var size = 0;
  var hashFn = function(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = ((hash << 5) - hash) + str.charCodeAt(i);
    }
    return hash;
  };
  var hash = function(key) {
    return hashFn(key) % size;
  };
  return {
    set: function(key, value) {
      items[hash(key)] = value;
      size++;
    },
    get: function(key) {
      return items[hash(key)];
    },
    has: function(key) {
      return items[hash(key)] !== undefined;
    },
    delete: function(key) {
      delete items[hash(key)];
      size--;
    },
    clear: function() {
      items = [];
      size = 0;
    },
    size: function() {
      return size;
    }
  };
};



var people = new HashTable(10);

people.set('bob', { age:4 });
people.set('charlie', { age:2 });
people.set('lucy', { age:1 });

console.log(people.size());
console.log(people.has('bob'));

people.delete('bob');

console.log(people.size());
console.log(people.has('bob'));

people.clear();
console.log(people.size());


// TODO: support objects as keys
// var obj = { foo:'bar' };
// people.set(obj, 'foobar');
// console.log(people.has(obj));
// console.log(people.get(obj));
