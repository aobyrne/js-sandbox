var utils = require('./utils');

var mergeSort = function(input) {
  if (input.length <= 1) {
    return input;
  } else {
    var middle = Math.floor(input.length / 2);
    var left = input.slice(0, middle);
    var right = input.slice(middle, input.length);
    left = mergeSort(left);
    right = mergeSort(right);
    return merge(left, right);
  }
};

var merge = function(left, right) {
  var result = [];
  while (left.length && right.length) {
    if (left[0] < right[0])
      result.push(left.shift());
    else
      result.push(right.shift());
  }
  while (left.length)
    result.push(left.shift());
  while (right.length)
    result.push(right.shift());
  return result;
};

console.time('mergeSort');
mergeSort(utils.getUnsortedNumbers(100000));
console.timeEnd('mergeSort');
