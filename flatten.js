var nestedArray = [1, 4, 'zzz', [], [[['a', 1, 500]]]]

var flattenES6 = function(input) {
  return input.reduce(
    (previous, current) =>
      Array.isArray(current)
        ? [...previous, ...flattenES6(current)]
        : [...previous, current],
    [],
  )
}

console.log('flatten:', flattenES6(nestedArray))
